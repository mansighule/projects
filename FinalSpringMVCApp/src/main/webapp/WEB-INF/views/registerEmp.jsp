<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
     <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
     <%@ page isELIgnored = "false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<%@ include file="head.jsp" %> 


 <!-- SweetAlert2 -->
  <link rel="stylesheet" href="<c:url value="/resources/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css"/>">
 
<!-- AdminLTE App -->
  
</head>
<body>

<%@ include file="menu.jsp" %>

<div class="content-wrapper">
 <section class="content">
      <div class="container-fluid">
        
           <div class="card card-primary">
              <div class="card-header">
                <h3  style="text-align:center">Employee Registration Form</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                
                <form action="/FinalSpringMVCApp/successpage" method="post" id="registerForm">
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <input type="hidden" name="empId" value="${emp.empId}" id="empid"/> 
                      
                      <input type="hidden" name="City" value="${emp.empCity}" id="cityname"/>
                      
                       <input type="hidden" id="ToastFlag" value="${ToastMsgFlag}"/> 
                       
                      <%--  <input type="hidden" id="PopupMsg" value="${btnName}"/> --%> 
                     	
                     	<input type="hidden" id="PopupMsg" value="${btnName}"/>
                      
                      
                      <div class="form-group">
                        <label>Full Name</label>
                        <input type="text" class="form-control" name="empFullName" placeholder="Required"  value="${emp.empFullName}">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" rows="2" name="empAddress" >${emp.empAddress}</textarea>
                      </div>
                    </div>
                  </div>
                 
                  <div class="row">
                    <div class="col-sm-6">
                      
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" name="empEmail" placeholder="Required"  value="${emp.empEmail}">
                      </div>
                    </div>
                    <div class="col-sm-6">
                     <div class="form-group">
                        <label>State</label>
                        <select class="form-control" name="stateId" id="state">
                        <option value="1">Choose State</option>
                         <c:forEach items="${stateList}" var="state">
                         
                         
                         <option value="${state.stateId}" <c:if test="${emp.stateId == state.stateId}">selected="selected"</c:if>>${state.stateName}</option>
                     
                         </c:forEach>
								
                        </select>
                      </div>
                      </div>
                    
                  </div>
                  
                  
                  <div class="row">
                    <div class="col-sm-6">
                    <div class="form-group">
                        <label>Contact No</label>
                        <input type="text" class="form-control" name="empContactNo" placeholder="Required"  value="${emp.empContactNo}">
                      </div>
                    </div>
                    
                   
                      
                      <div class="col-sm-6">
                      <div class="form-group">
                        <label>City</label>
                        <select class="form-control" name="empCity" id="city">
                         <option value="0">Choose City</option>
                        </select>
                      </div>
                    </div>
                      
                  </div>
                  
                                  
                  <div class="row">
                  <div class="col-sm-6">
                      <!-- checkbox -->
                      <div class="form-group">
                       <label>Language Known</label>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="English" name="empLanguage" <c:if test="${fn:contains(emp.empLanguage, 'English')}">checked="checked"</c:if> >
                          <label class="form-check-label">English</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="Hindi" name="empLanguage" <c:if test="${fn:contains(emp.empLanguage, 'Hindi')}">checked="checked"</c:if>>
                          <label class="form-check-label">Hindi</label>
                        </div>
                         <div class="form-check">
                          <input class="form-check-input" type="checkbox" value="Marathi" name="empLanguage" <c:if test="${fn:contains(emp.empLanguage, 'Marathi')}">checked="checked"</c:if>>
                          <label class="form-check-label">Marathi</label>
                        </div>
                        
                      </div>
                    </div>
                    
                    <div class="col-sm-6">
                      <!-- radio -->
                      <div class="form-group">
                      <label>Select Gender</label>
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio1" value="Male" name="empGender" <c:if test="${emp.empGender == 'Male'}">checked="checked"</c:if> >
                          <label for="customRadio1" class="custom-control-label">Male</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio2" value="Female" name="empGender" <c:if test="${emp.empGender == 'Female'}">checked="checked"</c:if>>
                          <label for="customRadio2" class="custom-control-label">Female</label>
                        </div>
                        
                      </div>
                    </div>
                    </div>
                  
                  
                  
                  <div class="row">
                  <div class="col-sm-6">
                      <!-- text input -->
                    <div class="form-group">
                        <label>How did you find us..?</label>
                        <select class="form-control" name="empFindUsOn">
                          <option value="PickOne" >Pick One</option>
								<option value="Google" <c:if test="${emp.empFindUsOn == 'Google'}">selected="selected"</c:if>>Google</option>
								<option value="Bing" <c:if test="${emp.empFindUsOn == 'Bing'}">selected="selected"</c:if>>Bing</option>
								<option value="Naukri.com" <c:if test="${emp.empFindUsOn == 'Naukri.com'}">selected="selected"</c:if>>Naukri.com</option>
								<option value="LinkedIn" <c:if test="${emp.empFindUsOn == 'LinkedIn'}">selected="selected"</c:if>>LinkedIn</option>
								<option value="Social Site" <c:if test="${emp.empFindUsOn == 'Social Site'}">selected="selected"</c:if>>Social Site</option>
								<option value="Friends/Workplace" <c:if test="${emp.empFindUsOn == 'Friends/Workplace'}">selected="selected"</c:if>>Friends/Workplace</option>
                        </select>
                      </div>
                      </div>
                      
                      
                       <div class="col-sm-6">
                      <div class="form-group">
                        <label>Short intro about your special skills..</label>
                        <textarea class="form-control" name="empSkills" >${emp.empSkills}</textarea>
                      </div>
                    </div>
                      
                    </div>
                   
                   <div class="card-footer" align="center">
                   <button type="button" class="btn btn-success btn-lg" id="btn">${btnName}</button>
                </div>
                   
                   <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="displayMsg"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                
              </button>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
              <button type="submit" class="btn btn-primary swalDefaultSuccess" id="submitBtn">Yes</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
                   
                   
                   
                </form>
              </div>
              <!-- /.card-body -->
            </div>


</div>
</section>
</div>
<%@ include file="footer.jsp" %>


<!-- SweetAlert2 -->
<script src="<c:url value="/resources/plugins/sweetalert2/sweetalert2.min.js"/>"></script>
 <!-- jquery-validation -->
<script src="<c:url value="/resources/plugins/jquery-validation/jquery.validate.min.js"/>"></script>
<script src="<c:url value="/resources/plugins/jquery-validation/additional-methods.min.js"/>"></script>


<script type="text/javascript">



$(function () {
	//alert("hi");
		
	  var stateId = $("#state").val();
		var cityName = $("#cityname").val();
		/* alert(stateId );
		alert(cityName); */
		
		$.ajax({
			type : "GET",
			url : "cityname/"+stateId+"/"+cityName,
			success : function(response){
				//alert("success");
				$("#city").html(response);
			}
    });
		

	 $("#state").on("change",function(){
         
         var stateId = $(this).val();
        
        // alert(stateId);
         //var stateData = {};
         //stateData["stateId"] = stateId;
         
         $.ajax({
                 type : "GET",                        
                 url : "cityname/"+stateId,
                 success : function(response){
                                 
                         var city = "<option value='0'>Select City</option>"
                         for (var i = 0; i < response.length; i++) {
                              
                                 
                                 city = city + "<option value='"+response[i].cityName+"'>"+response[i].cityName+"</option>";
                                 
                         }
                                                         
                         $("#city").html(city);
                         
                 }
         });
         
 });
	 
	 
	 /*  toast msg */
			const Toast = Swal.mixin({
			    toast: true,
			    position: 'top-end',
			    showConfirmButton: false,
			    timer: 6000
			  });
		
			 var ToastMsgFlag = $('#ToastFlag').val();
				
				if(ToastMsgFlag == 'add'){
					
					    Toast.fire({
					      icon: 'success',
					      title: 'Record Added Successfully.'
					    })
					 
				}
			
				
				
				$('#btn').on('click', function() {
					 
					 
					 if( $("#registerForm").valid() == true){
						 
						 $("#modal-default").modal("show");
			 
					 }
					   
					});
				
					/*pop up msg modal*/
				var PopupMsg = $('#PopupMsg').val();
				$('#modal-default').on('show.bs.modal', function(e){
			        $('#displayMsg').text("Are Your Sure Want To "+PopupMsg+" Record?");
			        
			        
			        $('#submitBtn').on('click', function(){
			        	$('#registerForm').submit();
			        	
			        });
			     
			    });
				
				
				$('#registerForm').validate({
				    rules: {
				      empEmail: {
				        required: true,
				        Empemail: true,
				      },
				      empFullName: {
				        required: true,
				      },
				      empAddress : {
				    	  required : true,
				      },
				      empCity : {
				    	  required : true,
				    	  
				      },
				      empContactNo : {
				    	  required : true,
				    	  minlength: 10,
				    	  
				      },
				      state : {
				    	  required : true,
				    	  
				      },
				      empLanguage : {
				    	  required : true,
				    	  
				      },
				      empGender: {
				    	  required : true,
				    	  
				      },
				      refer_from: {
				    	  required : true,
				    	  
				      },
				      empSkills : {
				    	  required : true,
				      },
				     
				    },
				    messages: {
				    	empEmail: {
				        required: "Please enter a Email Address",
				        email: "Please enter a vaild email address"
				      },
				      empFullName: "Please enter Full Name",
				    	  address: "Please enter Detail Address",
					    
					   
						   city: "Please enter a City",
						        
						
						   empContactNo :{
							   required: "Please enter a Contact No",
							   minlength: "Contact No must contain 10 digits"
						   },
					   
					   
					    	state: "Please enter a State",
					 
					   
						   empLanguage: "Please enter a Language",
						   
							empGender : "Please Enter a Gender",
							
							refer_from  : "Please Enter Where You Found Us on..?",
							
							empSkills : "Please Enter Skills"
						
				    },
				    errorElement: 'span',
				    errorPlacement: function (error, element) {
				      error.addClass('invalid-feedback');
				      element.closest('.form-group').append(error);
				    },
				    highlight: function (element, errorClass, validClass) {
				      $(element).addClass('is-invalid');
				    },
				  });
				
				
});


</script>
</body>
</html>