<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
     <%@ page isELIgnored = "false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<%@ include file="head.jsp" %> 

<!-- DataTables -->
  <link rel="stylesheet"  href="<c:url value="/resources/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css"/>">
  <link rel="stylesheet"  href="<c:url value="/resources/plugins/datatables-responsive/css/responsive.bootstrap4.min.css"/>">
  
  <!-- Toastr -->
  <link rel="stylesheet"  href="<c:url value="/resources/plugins/toastr/toastr.min.css"/>">
  
 <!-- SweetAlert2 -->
  <link rel="stylesheet"  href="<c:url value="/resources/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css"/>">
  
  
</head>
<body>
<%@ include file="menu.jsp" %>
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
<div class="card">
              <div class="card-header">
                <h1 style="text-align:center">Employee Data</h1>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <%-- <input type="hidden" id="deleteFlag" value="${ToastMsgFlag}"/> --%>
               <input type="hidden" id="ToastFlag" value="${ToastMsgFlag}"/> 
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                 
                     <tr>
      <th>Employee Id</th>
      <th>Full Name</th>
      <th>Email</th>
      <th>Address</th>
      <th>State</th>
      <th>City</th>
      <th>Contact No</th>
      <th>Language</th>
      <th>Gender</th>
      <th>Found Us On..?</th>
      <th>Special Skills</th>
      <th>Action</th>
     
    </tr>
                  </thead>
                  
                 
  <tbody>
  <c:forEach items="${empList}" var="l">
    <tr>
      <td >${l.empId}</td>
      <td >${l.empFullName}</td>
      <td >${l.empEmail}</td>
      <td >${l.empAddress}</td>
      <td >${l.stateName}</td>
      <td >${l.empCity}</td>
      <td >${l.empContactNo}</td>
      <td >${l.empLanguage}</td>
      <td >${l.empGender}</td>
      <td >${l.empFindUsOn}</td>
      <td >${l.empSkills}</td>
       <td><a href="update/${l.empId }">Edit</a>
       <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-default" data-id="${l.empId }">Delete</button></td>
      
       
<%--        <a href="EmpController?action=delete&empid=${l.empId}">
 --%>       
      
    </tr>
     </c:forEach>
  </tbody>
 
                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
        
          <!-- /.col -->
       </div>
        <!-- /.row -->
     </section>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

<!-- ./wrapper -->
<div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"> Do you want to delete this Record...?</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
              <a href="#" type="button" class="btn btn-primary toastrDefaultSuccess" id="btn">Yes</a>
               
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->


        
      <%@ include file="footer.jsp" %>
        <!-- DataTables -->
<script src="<c:url value="/resources/plugins/datatables/jquery.dataTables.min.js"/>"></script>
<script src="<c:url value="/resources/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"/>"></script>
<script src="<c:url value="/resources/plugins/datatables-responsive/js/dataTables.responsive.min.js"/>"></script>
<script src="<c:url value="/resources/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"/>"></script>
<!-- Toastr -->
<script src="<c:url value="/resources/plugins/toastr/toastr.min.js"/>"></script>

<!-- SweetAlert2 -->
<script src="<c:url value="/resources/plugins/sweetalert2/sweetalert2.min.js"/>"></script>
<script type="text/javascript">
$(function () {
	

	const Toast = Swal.mixin({
	    toast: true,
	    position: 'top-end',
	    showConfirmButton: false,
	    timer: 6000
	  });
	
	

	 var ToastMsgFlag = $('#ToastFlag').val();
		
	if(ToastMsgFlag == 'add'){
			
			    Toast.fire({
			      icon: 'success',
			      title: 'Record Added Successfully.'
	
			    })
			 
		}
	
	
	
	 
	 if(ToastMsgFlag == 'delt'){
    	
    	$(document).Toasts('create', {
            class: 'bg-success', 
            title: 'Message',
            autohide: true,
            body: 'Record Deleted Successfully..!',
            delay: 800
          })
    	
    }
       
	
	if(ToastMsgFlag == 'edit'){
		Toast.fire({
		      icon: 'success',
		      title: 'Record Edited Successfully.'
		    })
		 
		
	}
	
    $("#example1").DataTable({
    	 "paging": true,
         "lengthChange": true,
         "searching": true,
         "ordering": true,
         "info": true,
         "autoWidth": false,
         "responsive": true,
    });
   
    
    
    $('#modal-default').on('show.bs.modal', function(e){
        var data = $(e.relatedTarget).data('id');
        $('#btn').attr('href','delete/'+data);
    });

    
    
     
      
  
  
  
  });







</script>
</body>
</html>