<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    <%@ page isELIgnored = "false" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<%@ include file="head.jsp" %> 
<!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
</head>
<body>
<%@ include file="menu.jsp" %>

<div class="content-wrapper">
 <section class="content">
      <div class="container-fluid">
        
           <div class="card card-primary">
              <div class="card-header">
                <h3  style="text-align:center">Employee Report</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                
                <form method="post" action="reporthandler">
                  <div class="row">
                    <div class="col-sm-6">
                     <div class="form-group">
                        <label>Select State</label>
                         <select name="state" id="state" class="form-control">
					        <option>Choose State</option>
					        <c:forEach items="${stateList}" var="state">
					         <option value="${state.stateId}">${state.stateName}</option>
					        
					        </c:forEach>
						       </select>
		                  </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                       <label>Select City</label>
       <select name="cityName" id="city" class="form-control">
       
       <option>Choose City</option>
       
       </select>
       
                      </div>
                    </div>
                  </div>
                  <div align="center">
                  <button type="submit" class="btn btn-primary btn-md" id="showBtn">Show Employee Data</button>
                  </div>
      
      
     </form></div>
      </div>
      </section>
      <c:if test="${empReport.size() > 0}"> 
      <div class="card">
              
              <!-- /.card-header -->
              <div class="card-body">
             
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
      <th>Employee Id</th>
      <th>Full Name</th>
      <th>Email</th>
      <th>Address</th>
      
      <th>Contact No</th>
      <th>Language</th>
      <th>Gender</th>
      <th>Found Us On..?</th>
      <th>Special Skills</th>
     
     
    </tr>
                  </thead>
                  
                 
  <tbody>
  <c:forEach items="${empReport}" var="l">
    <tr>
      <td >${l.empId}</td>
      <td >${l.empFullName}</td>
      <td >${l.empEmail}</td>
      <td >${l.empAddress}</td>
      <td >${l.empContactNo}</td>
      <td >${l.empLanguage}</td>
      <td >${l.empGender}</td>
      <td >${l.empFindUsOn}</td>
      <td >${l.empSkills}</td>
       
       
<%--        <a href="EmpController?action=delete&empid=${l.empId}">
 --%>       
      
    </tr>
     </c:forEach>
  </tbody>
 
                  
                </table>
               
              </div>
              <!-- /.card-body -->
            </div>
            </c:if> 
            
      </div>
      


<%@ include file="footer.jsp" %>
<script src="plugins/jquery/jquery.min.js"></script>
<!-- DataTables -->
  <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>


<script type="text/javascript">


$(document).ready( function(){
	

	 
	 $("#state").on("change",function(){
        
        var stateId = $(this).val();
       //s alert(stateId);
        //var stateData = {};
        //stateData["stateId"] = stateId;
        
        $.ajax({
                type : "GET",                        
                url : "cityname/"+stateId,
                success : function(response){
                                
                        var options = "<option value='0'>Select City</option>"
                        for (var i = 0; i < response.length; i++) {
                                options += "<option value='"+response[i].cityName+"'>"+response[i].cityName+"</option>";
                        }
                                                        
                        $("#city").html(options);
                        
                }
        });
        
});
	
	
	
	$("#example1").DataTable({
   	 "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
   });
	
});

</script>
</body>
</html>