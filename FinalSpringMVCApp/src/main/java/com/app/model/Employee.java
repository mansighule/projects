package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "employee_data")
public class Employee {

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", stateId=" + stateId + ", empFullName=" + empFullName + ", empEmail="
				+ empEmail + ", empAddress=" + empAddress + ", empCity=" + empCity + ", empContactNo=" + empContactNo
				+ ", empLanguage=" + empLanguage + ", empGender=" + empGender + ", empFindUsOn=" + empFindUsOn
				+ ", empSkills=" + empSkills + ", stateName=" + stateName + "]";
	}





	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "empid")
	private int empId;
	
	
	@Column(name = "state_id")
	private int stateId;
	
	
	@Column(name = "fullname")
	private String empFullName;
	
	
	@Column(name = "email")
	private String empEmail;
	
	
	@Column(name = "address")
	private String empAddress;
	
	
	@Column(name = "city")
	private String empCity;

	
	@Column(name = "contactno")
	private String empContactNo;
	
	
	
	@Column(name = "language_known")
	private String empLanguage;
	
	
	@Column(name = "gender")
	private String empGender;
	
	
	@Column(name = "find_us_on")
	private String empFindUsOn;
	
	
	@Column(name = "special_skills")
	private String empSkills;
	
	@Transient
	private String stateName;
	
	
	
	

	public Employee(int empId, int stateId, String empFullName, String empEmail, String empAddress, String empCity,
			String empContactNo, String empLanguage, String empGender, String empFindUsOn, String empSkills,
			String stateName) {
		super();
		this.empId = empId;
		this.stateId = stateId;
		this.empFullName = empFullName;
		this.empEmail = empEmail;
		this.empAddress = empAddress;
		this.empCity = empCity;
		this.empContactNo = empContactNo;
		this.empLanguage = empLanguage;
		this.empGender = empGender;
		this.empFindUsOn = empFindUsOn;
		this.empSkills = empSkills;
		this.stateName = stateName;
	}





	public Employee() {
		super();
		// TODO Auto-generated constructor stub
	}





	public int getEmpId() {
		return empId;
	}





	public void setEmpId(int empId) {
		this.empId = empId;
	}





	public int getStateId() {
		return stateId;
	}





	public void setStateId(int stateId) {
		this.stateId = stateId;
	}





	public String getEmpFullName() {
		return empFullName;
	}





	public void setEmpFullName(String empFullName) {
		this.empFullName = empFullName;
	}





	public String getEmpEmail() {
		return empEmail;
	}





	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}





	public String getEmpAddress() {
		return empAddress;
	}





	public void setEmpAddress(String empAddress) {
		this.empAddress = empAddress;
	}





	public String getEmpCity() {
		return empCity;
	}





	public void setEmpCity(String empCity) {
		this.empCity = empCity;
	}





	public String getEmpContactNo() {
		return empContactNo;
	}





	public void setEmpContactNo(String empContactNo) {
		this.empContactNo = empContactNo;
	}





	public String getEmpLanguage() {
		return empLanguage;
	}





	public void setEmpLanguage(String empLanguage) {
		this.empLanguage = empLanguage;
	}





	public String getEmpGender() {
		return empGender;
	}





	public void setEmpGender(String empGender) {
		this.empGender = empGender;
	}





	public String getEmpFindUsOn() {
		return empFindUsOn;
	}





	public void setEmpFindUsOn(String empFindUsOn) {
		this.empFindUsOn = empFindUsOn;
	}





	public String getEmpSkills() {
		return empSkills;
	}





	public void setEmpSkills(String empSkills) {
		this.empSkills = empSkills;
	}





	public String getStateName() {
		return stateName;
	}





	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	
	
	
	
	
}
