package com.app.controller;

import java.util.List;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.app.model.City;
import com.app.model.Employee;
import com.app.model.State;
import com.app.service.EmpService;

//comment added

@Controller
public class EmployeeController {
	
	@Autowired
	private EmpService empService;
	
	@Autowired
	private SessionFactory factory;
	
	
	
	//view add page
	@RequestMapping("/")
	public String home() {
		
		return "index";
	}


	//add
	@RequestMapping(path= "/addEmp" , method = RequestMethod.GET)
	public ModelAndView addEmp() {
		ModelAndView modelAndView  = new ModelAndView();
		List<State> stateList = empService.viewState();
		modelAndView.addObject("stateList", stateList);
		modelAndView.addObject("btnName","Submit");
		
		modelAndView.setViewName("registerEmp");
		return modelAndView;
	}
	
	
	
	
		//view  emp
		@RequestMapping(path = "/viewEmp"  ,method = RequestMethod.GET)
		public ModelAndView viewEmp() {
			
		
		ModelAndView modelAndView  = new ModelAndView();
		List<Employee> empList = empService.viewEmp();
		
		modelAndView.addObject("empList", empList);
		modelAndView.setViewName("ViewEmp");
		
			return modelAndView;
		}
	
		
		
	//register handler
	@RequestMapping(path="/successpage" , method = RequestMethod.POST )
	public String registerHandler(@ModelAttribute Employee emp, BindingResult bindingResult ,RedirectAttributes redirectAttributes) {
		
		if(emp.getEmpId()==0) {
		
			empService.addEmp(emp);
		redirectAttributes.addFlashAttribute("ToastMsgFlag", "add");
		}else {
			empService.addEmp(emp);
		redirectAttributes.addFlashAttribute("ToastMsgFlag", "edit");
		}
		return "redirect:/viewEmp";
	
	
	}
	
	
	
	
	//update handler
	@RequestMapping(path = "/update/{empId}" , method = RequestMethod.GET)
	public ModelAndView updateHandler(@PathVariable("empId") int empId) {
		
		List<State>  stateList= empService.viewState();
		Employee emp1 = empService.getEmpId(empId);
		ModelAndView modelAndView  = new ModelAndView();
		
		modelAndView.addObject("emp",emp1);
		modelAndView.addObject("btnName","Update");
		modelAndView.addObject("stateList", stateList);
		
		modelAndView.setViewName("registerEmp");
		return modelAndView;
	}
	
	
	
	
	
	
	 //getCitybyStateId    handler(for ajax)
	@RequestMapping(path="/cityname/{stateId}", method = RequestMethod.GET)
    public @ResponseBody List<City> GetCityByState(@PathVariable("stateId") int stateId) {
            System.out.println(stateId);
            List<City> cityList = empService.getCityByStateId(stateId);
            
            return cityList; 
    }
	
	
	
	
	
	
	//onLoad   controller(for ajax)
	@RequestMapping(path=" cityname/{stateId}/{cityName}", method = RequestMethod.GET)
    public @ResponseBody String GetCityByStateOnLoad(@PathVariable("stateId") int stateId,
    														@PathVariable("cityName") String cityName) {
            //System.out.println(stateId);
            List<City> cityList = empService.getCityByStateId(stateId);
            
            String city = "";
    		//System.out.println("cityName "+cityName);
    		for (City c1 : cityList) {
    			
    			/*
    			 * city = city + "<option value="+c1.getCityName()
    			 * +"       <c:if test='${emp.empCity == "+c1.getCityName()
    			 * +"}'>selected='selected'</c:if> >"+c1.getCityName()+"</option>";
    			 */		
    			
    			if(cityName!=null) {
    					
    				if(cityName.equals(c1.getCityName())) {
    				
    				city = city + "<option value='"+c1.getCityName()+"' selected='selected'>"+c1.getCityName()+"</option>";
    				
    					}else {
    						
    						city = city + "<option value='"+c1.getCityName()+"'>"+c1.getCityName()+"</option>";
    					}
    			}
    			else {
    				
    			city = city + "<option value='"+c1.getCityName()+"'>"+c1.getCityName()+"</option>";
    			}
    		}
            
    		//System.out.println("controller"+city);
    	//	System.out.println("stateId " +stateId);
            return city; 
    }
	
	
	
	
	//delete 
	
	@RequestMapping(path = "/delete/{empId}" , method = RequestMethod.GET)
	public String deleteEmp(@PathVariable("empId") int empid,RedirectAttributes redirectAttributes) {
		
		empService.deleteEmp(empid);
		  
		redirectAttributes.addFlashAttribute("ToastMsgFlag", "delt");
		
		return "redirect:/viewEmp";
	}
	
	
	
	
	// view page
	@RequestMapping(path = "/report" ,method = RequestMethod.GET )
	
	public ModelAndView report() {
		
		  ModelAndView mv = new ModelAndView(); 
		  List<State> stateList = empService.viewState();
		  mv.addObject("stateList", stateList);
		  mv.setViewName("ReportByLocation");
		  return mv;
	}
	
	
	
	
	
	//report handler
	@RequestMapping(path ="/reporthandler" ,method = RequestMethod.POST)
	public ModelAndView empReportHandler(@RequestParam("cityName") String cityName) {
		
		ModelAndView mv = new ModelAndView();
		List<State> stateList = empService.viewState();
		mv.addObject("stateList", stateList);
		List<Employee> empReport = empService.getEmpReport(cityName);
		System.out.println("list" +empReport);
		mv.addObject("empReport",empReport);
		
		mv.setViewName("ReportByLocation");
		
		return mv;
		
	}

	
}
