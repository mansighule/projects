package com.app.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.model.City;
import com.app.model.Employee;
import com.app.model.State;
import com.app.repository.EmpRepository;


@Service
public class EmpServiceImpl implements EmpService {
	
	@Autowired
	private EmpRepository empRepo;

	@Override
	public void addEmp(Employee emp) {
		// TODO Auto-generated method stub
		
		empRepo.addEmp(emp);
		
	}

	@Override
	public List<Employee> viewEmp() {
		// TODO Auto-generated method stub
		
		List<Employee> viewEmp = empRepo.viewEmp();
		
		return viewEmp;
	}

	@Override
	public void deleteEmp(int empId) {
		// TODO Auto-generated method stub
		
		empRepo.deleteEmp(empId);
		
		
	}

	@Override
	public Employee getEmpId(int empid) {
		// TODO Auto-generated method stub
		
		Employee empId = empRepo.getEmpId(empid);
		
		return empId;
	}


	@Override
	public List<State> viewState() {
		// TODO Auto-generated method stub
		
		List<State> viewStateList = empRepo.viewState();
		return viewStateList;
	}

	@Override
	public List<City> getCityByStateId(int stateId) {
		
		List<City> cityList = empRepo.getCityByStateId(stateId);
		return cityList;
	}

	@Override
	public List<Employee> getEmpReport(String cityName) {
		// TODO Auto-generated method stub
		
		
		List<Employee> empReport = empRepo.getEmpReport(cityName);
		return empReport;
	}

	
}
