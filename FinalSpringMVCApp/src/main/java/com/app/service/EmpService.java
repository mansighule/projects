package com.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.app.model.City;
import com.app.model.Employee;
import com.app.model.State;


@Service
public interface EmpService {

	

	public void addEmp(Employee emp);
	
	public List<Employee> viewEmp();
	
	public void deleteEmp(int empId);
	
	public Employee getEmpId(int empid);
	
	public List<State> viewState();
	
	public List<City> getCityByStateId(int stateId);
	
	public List<Employee> getEmpReport(String cityName);
	
	
}
