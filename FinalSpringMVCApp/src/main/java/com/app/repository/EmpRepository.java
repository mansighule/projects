package com.app.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.app.model.City;
import com.app.model.Employee;
import com.app.model.State;


@Repository
public interface EmpRepository {
	
	
	public void addEmp(Employee emp);
	
	public List<Employee> viewEmp();
	
	public void deleteEmp(int empId);
	
	public Employee getEmpId(int empid);

	public List<State> viewState();
	
	public List<City> getCityByStateId(int stateId);

	public List<Employee> getEmpReport(String cityName);
	
}
