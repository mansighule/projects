package com.app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.app.model.City;
import com.app.model.Employee;
import com.app.model.State;

@Repository
public class EmpRepositoryImpl implements EmpRepository{

	@Autowired
	private HibernateTemplate template;
	
	@Autowired
	private SessionFactory factory;
	
	
	
	// add
	@Transactional
	@Override
	public void addEmp(Employee emp) {
		// TODO Auto-generated method stub
		
		template.saveOrUpdate(emp);
		
	}
	
	
	
	//view
	@Override
	public List<Employee> viewEmp() {
		// TODO Auto-generated method stub
		
		Session session = factory.openSession();
		 String view =
				  "select  e.empId as empId, e.empFullName as empFullName, e.empEmail as empEmail , e.empAddress as empAddress, e.empCity as empCity, e.empContactNo as empContactNo, e.empLanguage as empLanguage, e.empGender as empGender,"
				  +
				  "e.empFindUsOn as empFindUsOn, e.empSkills as empSkills, s.stateName as stateName from Employee e,State s where e.stateId=s.stateId ";
				 
				Query q = session.createQuery(view); 
				
				@SuppressWarnings({ "unchecked", "deprecation" })
				List<Employee> empList= q.setResultTransformer(Transformers.aliasToBean(Employee.class)).list();
		
		return empList;
	}

	//delete
	@Transactional
	@Override
	public void deleteEmp(int empid) {
		// TODO Auto-generated method stub
		Employee employee = template.get(Employee.class,empid);
		template.delete(employee);
		
		
		
	}

	//get single obj(from table)
	@Override
	public Employee getEmpId(int empid) {
		// TODO Auto-generated method stub
		Employee employee = template.get(Employee.class,empid);
		
		return employee;
	}

	
	//
	@Override
	public List<State> viewState() {
		// TODO Auto-generated method stub
		
		List<State> stateList = template.loadAll(State.class);
		
		return stateList;
	}

	@Override
	public List<City> getCityByStateId(int stateId) {
		
		Session session = factory.openSession();
		
		
		
		Query q = session.createQuery("from City where state.stateId=:id");
		
		q.setParameter("id", stateId);
		
		List<City> cityList= q.list();
		System.out.println("Repo cityList:" +cityList );
		return cityList;
	}

	
	
	@Override
	public List<Employee> getEmpReport(String cityName) {
		// TODO Auto-generated method stub
		
		
		Session session = factory.openSession();
		
		
		Query q = session.createQuery("from Employee where empCity=:c");
		
		q.setParameter("c",cityName);
		
		List<Employee> empList= q.list();
		//System.out.println("reportlist"+ empList);
		return empList;
	}


}
